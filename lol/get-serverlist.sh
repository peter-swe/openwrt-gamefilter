#!/bin/sh

BASEDIR=$(dirname $0)
INSTALLTMP=/var/lolinstall-$$
mkdir ${INSTALLTMP}

#
# League of legends server lists (as found presently)
#
NA="https://support.leagueoflegends.com/attachments/token/l8gztqrh95gsmhk/?name=NA+Complete+Ranges.xlsx"
EUW="https://support.leagueoflegends.com/attachments/token/7dfwzsvmnq2upbe/?name=EUW+Complete+Ranges.xlsx"
EUNE="https://support.leagueoflegends.com/attachments/token/xqdxitg2lpby5tw/?name=EUNE+Complete+Ranges.xlsx"

#
# this wget requires better wget than busybox implementation
# opkg wget openssl-util
#
wget --no-check-certificate ${NA} -O ${INSTALLTMP}/NA.xlsx
wget --no-check-certificate ${EUW} -O ${INSTALLTMP}/EUW.xlsx
wget --no-check-certificate ${EUNE} -O ${INSTALLTMP}/EUNE.xlsx

#
# Conversion from xslx to csv files
# opkg python python-expat
# 
${BASEDIR}/xlsx2csv.py ${INSTALLTMP}/NA.xlsx   ${INSTALLTMP}/NA.csv
${BASEDIR}/xlsx2csv.py ${INSTALLTMP}/EUW.xlsx  ${INSTALLTMP}/EUW.csv
${BASEDIR}/xlsx2csv.py ${INSTALLTMP}/EUNE.xlsx ${INSTALLTMP}/EUNE.csv

#
# Merge all serves into CIDR subranges (cidr.txt)
# busybox sort does not do this to its full extent
#
head -n 2 ${INSTALLTMP}/NA.csv | tail -n 1 | sed 's/,/\n/g' >> ${INSTALLTMP}/cidrtmp.txt
head -n 2 ${INSTALLTMP}/EUW.csv | tail -n 1 | sed 's/,/\n/g' >> ${INSTALLTMP}/cidrtmp.txt
head -n 2 ${INSTALLTMP}/EUNE.csv | tail -n 1 | sed 's/,/\n/g' >> ${INSTALLTMP}/cidrtmp.txt
sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 ${INSTALLTMP}/cidrtmp.txt | uniq > ${BASEDIR}/cidr.txt

#
# Merge all servers into ip-list (ip.txt)
# Differences exists in files...
#
tail -n +3 ${INSTALLTMP}/NA.csv | sed 's/,/\n/g' >> ${INSTALLTMP}/iptmp.txt
tail -n +4 ${INSTALLTMP}/EUW.csv | sed 's/,/\n/g' >> ${INSTALLTMP}/iptmp.txt
tail -n +3 ${INSTALLTMP}/EUNE.csv | sed 's/,/\n/g' >> ${INSTALLTMP}/iptmp.txt
sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 ${INSTALLTMP}/iptmp.txt | uniq > ${BASEDIR}/ip.txt
rm -rf ${INSTALLTMP}
