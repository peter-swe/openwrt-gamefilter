#!/bin/sh
#
# Usage: filter.sh OK|WARN|BLOCK hostname
#
BASEDIR=$(dirname "$0")

#
# Verify common rules
#
MY_IP_SET=my_set
ipset list -n | grep ${MY_IP_SET} > /dev/null 2>&1
if [ $? -eq 1 ]; then
	# Add ipset
	ipset create ${MY_IP_SET} hash:net
	for CIDR in $(cat ${BASEDIR}/lol/cidr.txt)
	do
		ipset add ${MY_IP_SET} "${CIDR}"
	done
fi

MY_BLOCK_LIST=my_block_list
iptables -L ${MY_BLOCK_LIST} > /dev/null
if [ $? -eq 1 ]; then
	# Add main block list entry
	iptables -N ${MY_BLOCK_LIST}
	iptables -I FORWARD -j ${MY_BLOCK_LIST}
fi

MY_BLOCK1=my_block1
iptables -L ${MY_BLOCK1} > /dev/null 2>&1
if [ $? -eq 1 ]; then
	# Add block limit 1
	iptables -N ${MY_BLOCK1}
	iptables -A ${MY_BLOCK1} -m limit --limit 20/s -m set --match-set ${MY_IP_SET} dst -j zone_wan_ACCEPT
	iptables -A ${MY_BLOCK1} -m set --match-set ${MY_IP_SET} dst -j zone_wan_DROP
fi

MY_BLOCK2=my_block2
iptables -L ${MY_BLOCK2} > /dev/null 2>&1
if [ $? -eq 1 ]; then
	# Add block limit 2
	iptables -N ${MY_BLOCK2}
	iptables -A ${MY_BLOCK2} -m set --match-set ${MY_IP_SET} dst -j zone_wan_DROP
fi

[ -z "${2}" ] && echo "ERROR : Missing argument" && exit 1

HOSTNAME=${2}

deactivate()
{
	[ -z "${1}" ] && echo "ERROR : deactivate, missing argument" && exit 1
	BLOCK=${1}
	iptables -L ${MY_BLOCK_LIST} --line-numbers | grep ${HOSTNAME} | grep ${BLOCK} | cut -d ' ' -f 1 | sort -r | while read NR
	do
		iptables -D ${MY_BLOCK_LIST} ${NR}
	done
}
activate()
{
	[ -z "${1}" ] && echo "ERROR : activate, missing argument" && exit 1
	BLOCK=${1}
	iptables -I ${MY_BLOCK_LIST} -j ${BLOCK} -s ${HOSTIP} -m comment --comment "${HOSTNAME}"
}

case ${1} in

"OK")
	echo "OK"
	iptables -L ${MY_BLOCK_LIST} | grep ${HOSTNAME} > /dev/null
	if [ $? -eq 0 ]; then
		deactivate ${MY_BLOCK1}
		deactivate ${MY_BLOCK2}
	fi
	;;

"WARN")
	HOSTIP=$(grep ${HOSTNAME} /var/dhcp.leases | cut -d ' ' -f 3)
	[ -z "${HOSTIP}" ] && echo "ERROR : IP unresolved" && exit 1
	echo "WARN"
	iptables -L ${MY_BLOCK_LIST} | grep ${MY_BLOCK1} | grep ${HOSTNAME} > /dev/null
	if [ $? -eq 1 ]; then
		deactivate ${MY_BLOCK2}
		activate ${MY_BLOCK1}
	fi
	;;
	
"BLOCK")
	HOSTIP=$(grep ${HOSTNAME} /var/dhcp.leases | cut -d ' ' -f 3)
	[ -z "${HOSTIP}" ] && echo "ERROR : IP unresolved" && exit 1
	echo "BLOCK"
	iptables -L ${MY_BLOCK_LIST} | grep ${MY_BLOCK2} | grep ${HOSTNAME} > /dev/null
	if [ $? -eq 1 ]; then
		deactivate ${MY_BLOCK1}
		activate ${MY_BLOCK2}
	fi
	;;
	
"REMOVE")
	echo "REMOVE"
	iptables -F ${MY_BLOCK_LIST}
	iptables -F ${MY_BLOCK1}
	iptables -X ${MY_BLOCK1}
	iptables -F ${MY_BLOCK2}
	iptables -X ${MY_BLOCK2}
	ipset destroy ${MY_IP_SET}
	;;
	
*)
	echo "Usage.."
	;;
esac
