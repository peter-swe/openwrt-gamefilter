#!/bin/sh

git archive master | gzip > openwrt-gamefilter.tar.gz
scp -r openwrt-gamefilter.tar.gz root@netgearopenwrt:

echo ""
echo "On openwrt type:"
echo "mkdir openwrt-gamefilter 2> /dev/null; cd $_"
echo "tar -xzf ../openwrt-gamefilter.tar.gz"
echo "./install.sh"

rsh root@netgearopenwrt "mkdir openwrt-gamefilter 2> /dev/null; cd openwrt-gamefilter;tar -xzf ../openwrt-gamefilter.tar.gz"

