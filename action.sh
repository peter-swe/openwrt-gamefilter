#!/bin/sh

GAMERS=/var/log/gamers.log
TMP=/var/log/gamers-$$.log
HTML=/var/log/gamers.htm

case $(date | cut -f 1 -d' ') in 
"Sun" | "Fri" | "Sat")
	WARNTIME=120
	BLOCKTIME=150
	;;
*)
	WARNTIME=60
	BLOCKTIME=80
	;;
esac

BASEDIR=$(dirname "$0")

#
# LuCI / HTML header
#
echo "<%+header%>" >> ${TMP}
echo "<h1><%:Gaming%></h1>" >> ${TMP}
echo "<br />" >> ${TMP}
echo "Datum <% print(os.date()) %><br />" >> ${TMP}
echo "<br/>" >> ${TMP}
echo "<table>" >> ${TMP}

if [ "$(date | cut -f 1-3 -d' ')" = "Thu May 29" -o "$(date | cut -f 1-3 -d' ')" = "Fri May 30" ]; then
	echo "</table>" >> ${TMP}
	echo "<h2><%:Free playtime!%></h2>" >> ${TMP}
	echo "<%+footer%>" >> ${TMP}
	rm -f ${HTML}
	mv ${TMP} ${HTML}
	exit
fi

#
# Classify gamers
# Remove from gamers when OK
#
cat ${GAMERS} | while read WHO
do
	PLAYER="/var/log/${WHO}.log"
	PLAYTIME=$(tail -1 ${PLAYER} | cut -d ' ' -f 1)
	ACTION=$(tail -1 ${PLAYER} | cut -d ' ' -f 2)
	PLAYERSTATE="/var/log/${WHO}_state.log"
	
	if [ ! -e ${PLAYERSTATE} ]; then
		echo "OK" > ${PLAYERSTATE}
	fi
	
	case $(cat ${PLAYERSTATE}) in
	
	"OK")
		if [ ${PLAYTIME} -gt ${WARNTIME} ]; then
			logger "gamefilter: ${WHO} is now WARNED"
			echo "WARN ${ACTION}" > ${PLAYERSTATE}
			${BASEDIR}/filter.sh WARN ${WHO}
		fi
		;;
	
	"WARN")
		if [ ${PLAYTIME} -eq 0 ]; then
			logger "gamefilter: ${WHO} is now OK"
			echo "OK" > ${PLAYERSTATE}
			${BASEDIR}/filter.sh OK ${WHO}
		elif [ ${PLAYTIME} -gt ${BLOCKTIME} ]; then
			logger "gamefilter: ${WHO} is now BLOCKED"
			echo "BLOCK ${ACTION}" > ${PLAYERSTATE}
			${BASEDIR}/filter.sh BLOCK ${WHO}
		fi
		;;
		
	"BLOCK")
		if [ ${PLAYTIME} -eq 0 ]; then
			logger "gamefilter: ${WHO} is now OK"
			echo "OK" > ${PLAYERSTATE}
			${BASEDIR}/filter.sh OK ${WHO}
		fi
		;;
	
	*)
		echo "Unclear state.."
		;;
	esac
	
	#
	# LuCI / HTML table contents
	#
	echo "<tr><td>${WHO}</td>" >> ${TMP}
	echo "<td>${PLAYTIME}</td>" >> ${TMP}
	echo "<td>$(cat ${PLAYERSTATE})</td></tr>" >> ${TMP}
	
done

#
# LuCI / HTML header
#
echo "</table>" >> ${TMP}
echo "<br/>" >> ${TMP}
echo "Warntime: ${WARNTIME}<br/>" >> ${TMP}
echo "Blocktime: ${BLOCKTIME}<br/>" >> ${TMP}
echo "Gametime must reach 0 to reset WARN/BLOCK<br/>" >> ${TMP}
echo "<%+footer%>" >> ${TMP}

rm -f ${HTML}
mv ${TMP} ${HTML}
