#!/bin/sh

GAMELOG=/var/log/gaming.log
PRESENT=/var/log/present-gamers.log
GAMERS=/var/log/gamers.log
TMP=/var/log/gamers-$$.log
TIMELIMIT=300

diff_min() {
	d1=$(date -d "$1" +%s)
	d2=$(date -d "$2" +%s)
	let diffmin=$(expr ${d2} - ${d1})/60
	echo ${diffmin}
}

# Create PRESENT as empty file
[ -e ${PRESENT} ] && rm ${PRESENT}
touch ${PRESENT}

#
# Add present gamers to PRESENT
#
TNOW=$(date +"%F %T")
touch ${TMP}
sort -r ${GAMELOG} | head -10 | while read DATE TIME WHO GAME
do
	TM="${DATE} ${TIME}"
	DIFF=$(diff_min "${TM}" "${TNOW}")
	
	if [ ${DIFF} -gt ${TIMELIMIT} ]; then
		break
	fi

	echo ${WHO} >> ${TMP}
done
sort ${TMP} | uniq > ${PRESENT}

#
# Add PRESENT to GAMERS
#
cat ${PRESENT} ${GAMERS} > ${TMP}
sort ${TMP} | uniq > ${GAMERS}
rm ${TMP}

#
# Find out present gaming time for GAMERS
#
TNOW=$(date +"%F %T")
cat ${GAMERS} | while read WHO
do
	LAST=${TNOW}
	START=${TNOW}
	DIFFNOW=0
	FIRST=1
	PLAYTIME=0
	PLAYER="/var/log/${WHO}.log"
	[ -e ${PLAYER} ] && rm ${PLAYER}
	touch ${PLAYER}
	grep ${WHO} ${GAMELOG} | sort -r > ${TMP}
	while read DATE TIME COMPUTER ACTION
	do
		TM="${DATE} ${TIME}"
		DIFFNOW=$(diff_min "${TM}" "${TNOW}")
		DIFFLAST=$(diff_min "${TM}" "${LAST}")

		echo "Debug ${TM} ${DIFFNOW} ${DIFFLAST} ${PLAYTIME}" >> ${PLAYER}
	
		if [ ${DIFFLAST} -gt 15 ]; then
			if [ ${FIRST} -eq 1 ]; then
				echo "${TNOW} Not playing for ${DIFFLAST} minutes" >> ${PLAYER}
			else
				DIFF=$(diff_min "${LAST}" "${START}")
				PLAYTIME=$(( PLAYTIME + DIFF ))
				echo "${LAST} ${START} Playtime continuous for ${DIFF} minutes, acc ${PLAYTIME}" >> ${PLAYER}
			fi
			START=${TM}
		else
			if [ ${DIFFNOW} -gt ${TIMELIMIT} ]; then
				break
			fi
		fi

		FIRST=0
		LAST=${TM}
	done < ${TMP}
	rm ${TMP}
	
	DIFF=$(diff_min "${LAST}" "${START}")
	PLAYTIME=$(( PLAYTIME + DIFF ))
	echo "Stopping search at ${DIFFNOW} minutes" >> ${PLAYER}
	echo "${PLAYTIME} ${ACTION}" >> ${PLAYER}
done
