#!/bin/sh

BASEDIR=$(dirname $0)
FULLDIR=$(cd $(dirname -- "$0") > /dev/null; pwd)

#
# Check if we are running on openwrt
#
if [ ! -e /etc/openwrt_version ]; then
	echo "ERROR: run install only on target."
	exit 1
fi

#
# Install packages required by gamefilter
#
if [ ! -e ${BASEDIR}/.pkg.installed ]; then
	echo "Installing packages"
	opkg update
	opkg install netstat-nat python python-expat wget openssl-util ipset
	touch ${BASEDIR}/.pkg.installed
fi

#
# Install LOL server list
#
if [ ! -e ${BASEDIR}/lol/ip.txt ]; then
	echo "Fetching LOL server list"
	${BASEDIR}/lol/get-serverlist.sh
fi

#
# Add entry to crontab
#
if [ ! -e /etc/crontabs/root ]; then
	touch /etc/crontabs/root
fi
grep gamefilter.sh /etc/crontabs/root > /dev/null
if [ $? -eq 1 ]; then
	# Add entry
	echo "*/10 * * * * ${FULLDIR}/gamefilter.sh" >> /etc/crontabs/root
fi
/etc/init.d/cron start

#
# Install lua/luci web support
#
tar xvzf lua_luci.tar.gz /
