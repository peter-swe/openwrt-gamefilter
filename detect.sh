#!/bin/sh

BASEDIR=$(dirname $0)
GAMELOG=/var/log/gaming.log
TIMETMP=/var/log/time.log
TIMESTOP=22   # Classified as start of gaming
TIMESTART=8  # Classified as stop of gaming
touch ${GAMELOG}

#
# LOL
#
LOLTMP=/var/lol-conn-$$.log
touch ${LOLTMP}
touch ${TIMETMP}
# Find ongoing connections
netstat-nat -o | sed 's/:/\t/g' | while read PROT SRC SRCPRT DEST DSTPRT STUFF
do
	# Search for predefined ip adresses
	grep ${DEST} $BASEDIR/lol/ip.txt
	if [ $? -eq 0 ]; then
		# DEST is found
		# save source address 		
		echo ${SRC} >> ${LOLTMP}
	fi
	
	# Time rule
	grep ${SRC} $BASEDIR/users/restrict.txt
	if [ $? -eq 0 ]; then
		# User in restrict is found in SRC
		HOUR=$(date +"%H")
		#if [ ${HOUR} -ge ${TIMESTOP} -a ${HOUR} -lt ${TIMESTART} ]; then
		if [ ${HOUR} -ge ${TIMESTOP} -o ${HOUR} -lt ${TIMESTART} ]; then
			# Time is up
			echo ${SRC} >> ${TIMETMP}
		fi
	fi
done

sort ${LOLTMP} | uniq | sed 's/\./\t/' | while read USER STUFF
do
	echo $(date +"%F %T") ${USER} LOL >> ${GAMELOG}
done

rm -f ${LOLTMP}

sort ${TIMETMP} | uniq | sed 's/\./\t/' | while read USER STUFF
do
	echo $(date +"%F %T") ${USER} TIME >> ${GAMELOG}
done

rm -f ${TIMETMP}
